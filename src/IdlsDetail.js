import React from "react";
import $ from "jquery";
import { Link} from "react-router";

/* creating IdlsDetail Class */
var IdlsDetails = React.createClass({

    /* setting initial state */
    getInitialState : function () {
        return {
            name: "",
            supername: "",
            properties: []
        };
    },

    /* getting data from api */
    componentDidMount: function () {
        var url = "/api/idls/" + this.props.params.detail;

        this.serverRequest = $.get(url, function (result) {
            this.setState(result);
        }.bind(this));
    },

    render : function() {
        var idlsDetail = this.state;

        return (
            <div>

                {/* idls name */}
                <h1>
                    <Link to="/idls" className="no-underline">(</Link>
                    {idlsDetail.name}
                    <span className="giveMeLightBlue">)</span>
                </h1>

                {/* super name */}
                <h2><small>Super Name</small></h2>
                <div className="well well-sm">
                    {idlsDetail.supername}
                </div>

                {/* properties */}
                {(idlsDetail.properties.length > 0) &&
                    <div>
                        <h2>
                            <small>Properties</small>
                        </h2>
                            {idlsDetail.properties.map( (elem, i)=> (
                                <div className="well well-sm" key={elem.name + "_" + i}>
                                    <h4>Name : <small>{elem.name}</small> </h4>
                                    <h4>Type : <small>{elem.type}</small> </h4>
                                    <h4>Attributes : {elem.attrs.map( (e, j)=> (<small key={e + "_" + j}>{j > 0 ? ", " + e : e} </small> ) )}</h4>
                                </div>
                            )
                        )}
                    </div>
                }

            </div>
        );
    }
});

module.exports = IdlsDetails;