import React from "react";
import $ from "jquery";
import { Link} from "react-router";
import { ButtonToolbar, DropdownButton, MenuItem } from "react-bootstrap";
/* creating TagsDetail Class */
var TagsDetail  = React.createClass({

    toggleGlobalEvents : function () {
        this.setState({
          isGlobalEventsOn : !this.state.isGlobalEventsOn
        });
    },

    toggleGlobalCore : function () {
        this.setState({
            isGlobalCoreOn : !this.state.isGlobalCoreOn
        });
    },

    /* setting initial state */
    getInitialState : function () {
        return {
            name: "",
            description: "",
            attributes: [],
            categories: [],
            examples: [],
            notes: [],
            recommendations: [],
            isGlobalCoreOn: false,
            isGlobalEventsOn: false
        };
    },

    /* getting data from api */
    componentDidMount: function () {
        var url = "/api/tags/" + this.props.params.detail;

        this.serverRequest = $.get(url, function (result) {
            this.setState(result);
        }.bind(this));
    },

    render : function() {
        var tagDetail =  this.state;
        var globalCores = ["accesskey", "class", "contenteditable", "contextmenu", "dir", "draggable", "dropzone",
                            "hidden", "id", "lang", "spellcheck", "style", "tabindex", "title", "translate"];
        var globalEvents = ["onabort", "onblur", "oncanplay", "oncanplaythrough", "onchange", "ondblclick", "ondrag",
                             "ondragend", "ondragenter", "ondragleave", "ondragover", "ondragstart", "ondrop",
                             "ondurationchange", "onemptied", "onended", "onerror", "onfocus", "onformchange",
                             "onforminput", "oninput", "oninvalid", "onkeydown", "onkeypress", "onkeyup",
                             "onload", "onloadeddata", "onloadedmetadata", "onloadstart", "onmousedown",
                             "onmousemove", "onmouseout", "onmouseover", "onmouseup", "onmousewheel",
                             "onpause", "onplay", "onplaying", "onprogress", "onratechange", "onreadystatechange",
                             "onscroll", "onseeked", "onseeking", "onselect", "onshow", "onstalled", "onsubmit",
                             "onsuspend", "ontimeupdate", "onvolumechange", "onwaiting" ];

        return (
            <div>

                {/* tag name */}
                <div >
                    <h1 className="makeInline" >
                        <Link to="/tags" className="no-underline">&lt;</Link>
                        {tagDetail.name}
                        <span className="giveMeLightBlue">&gt;</span>
                    </h1>

                    <ButtonToolbar className="makeInline">
                        <DropdownButton title="   Global" id="bg-nested-dropdown" className="glyphicon glyphicon-cog" bsStyle="success">
                            <MenuItem eventKey="1" onClick={this.toggleGlobalCore}>
                                Global : core
                            </MenuItem>
                            <MenuItem eventKey="2" onClick={this.toggleGlobalEvents}>
                                Global : events
                            </MenuItem>
                        </DropdownButton>
                    </ButtonToolbar>
                 </div>

                {/* description */}
                <h2><small>Description</small></h2>
                <div className="well well-sm">
                    <span dangerouslySetInnerHTML={{__html: tagDetail.description}} />
                </div>

                {/* attributes */}
                {(tagDetail.attributes.length > 0) &&
                    <div>
                        <h2><small>Attributes</small></h2>
                        <div className="well well-sm">
                            {tagDetail.attributes.map( (elem, i) => (<Link to={"/attributes/" + elem.name} key={elem.name + "_" + i} className="button btn-primary btn-sm">{elem.name}</Link>))}
                        </div>
                    </div>
                }

                {/* Global Core*/}
                { tagDetail.isGlobalCoreOn &&
                    <div>
                        <h2><small>Global Attributes : Core</small></h2>
                        <div className="well well-sm">
                            {globalCores.map( (elem, i) => (<Link to={"/attributes/" + elem} key={elem + "_" + i} className="button btn-primary btn-sm">{elem}</Link>))}
                        </div>
                    </div>
                }

                {/* Global Events*/}
                { tagDetail.isGlobalEventsOn &&
                    <div>
                        <h2><small>Global Attributes : Event-handler</small></h2>
                        <div className="well well-sm">
                            {globalEvents.map( (elem, i) => (<Link to={"/attributes/" + elem} key={elem + "_" + i} className="button btn-primary btn-sm">{elem}</Link>))}
                        </div>
                    </div>
                }

                {/* examples */}
                {tagDetail.examples.map( (elem, i) => (
                    <span key={elem.title + "_" + i}>
                        <h2><small>Example: {elem.title}</small></h2>
                        <pre>{elem.value}</pre>
                        <div className="well well-sm">
                            <span dangerouslySetInnerHTML={{__html:elem.value}} />
                        </div>
                    </span>)
                )}

                {/* notes */}
                {(tagDetail.notes.length > 0) &&
                    <div>
                        <h2><small>Notes</small></h2>
                        <div className="well well-sm">
                            {tagDetail.notes.map( (elem, i) => ( <span key={elem.value + "_" + i} dangerouslySetInnerHTML={{__html:elem.value}} />))}
                        </div>
                    </div>
                }

                {/* recommendations */}
                {(tagDetail.recommendations.length > 0) &&
                    <div>
                        <h2><small>Recommendations</small></h2>
                        <div className="well well-sm">
                            {tagDetail.recommendations.map( (elem, i) => ( <span key={elem.value + "_" + i} dangerouslySetInnerHTML={{__html:elem.value}} />))}
                        </div>
                    </div>
                }




            </div>
        );
    }
});

module.exports = TagsDetail;