import React from "react";
import $ from "jquery";
import { Link} from "react-router";

/* creating Attributes view */
var Attributes = React.createClass({

    /* setting initial state */
    getInitialState : function () {
        return {
            attributesList: [],
            originalList: []
        };
    },

    filterList: function (event) {
        var updatedList = this.state.originalList;
        updatedList = updatedList.filter(function(attribute){
            return attribute.toLowerCase().search(
                    event.target.value.toLowerCase()) !== -1;
        });
        this.setState({attributesList: updatedList});
    },
    /* getting data from api */
    componentDidMount: function () {
        var url = "/api/attributes/";

        this.serverRequest = $.get(url, function (result) {
            this.setState({
                attributesList: result.sort(),
                originalList: result.sort()
            });
        }.bind(this));
    },

    render : function() {
        var attributeNames = this.state.attributesList.map(function (name, i) {
            return(
                <li key={name + "_" + i}  data-category={name}>
                    <Link to={"/attributes/" + name}>
                        {"[" + name + "]"}
                    </Link>
                </li>
            );
        });

        return (
            <div>
                {/* attributes */}
                <ul className="no-list-style">
                    <li><input type="text" placeholder="Search" onChange={this.filterList}/><br/><br/></li>
                    {attributeNames}
                </ul>
            </div>
        );
    }
});

module.exports = Attributes;
