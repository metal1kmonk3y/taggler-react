import React from "react";
import { Link } from "react-router";

/* creating NavMenu Class */
var NavMenu = React.createClass({

        render: function(){
            var views = ["/", "tags", "attributes", "idls"];
            var activeTab = window.location.pathname.split("/")[1] || "/" ;

            var viewLinks = views.map(function(view, i){
                var isActive = activeTab == view ? "active" : "";

                return (
                    <li key={"view_" + i} id={view} className={isActive}>
                        <Link to={i == 0 ? view : "/" + view}>

                            { i == 0 ? "Home" :  view.charAt(0).toUpperCase() + view.slice(1) }
                        </Link>

                    </li>
                );
            });

            return (
                <div>
                    <nav className="navbar navbar-fixed-top navbar-inverse">
                        <Link className="navbar-brand no-list-style" to="/">
                            Taggler
                        </Link>
                        <ul className="nav navbar-nav" id="nav-list">
                            {viewLinks}
                        </ul>
                    </nav>
                </div>
            );
        }
});

module.exports = NavMenu;