import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import NavMenu from './NavMenu';
import Tags from './Tags'
import TagsDetail from './TagsDetail';
import Attributes from './Attributes';
import AttributesDetail from './AttributesDetail';
import Idls from './Idls';
import IdlsDetail from './IdlsDetail';

/* creating Home view */
function Home (){
        return (
            <div>                
                <div className="container">
                    <h1>Taggler </h1><br />
                    <h3>An HTML5 Reference</h3><br />
                    <h4>Just the Facts</h4><br />
                    <p>Many online HTML references contain so many details that it can be difficult to find the basic
                        information you need. We have stripped out details related to platform support, previous HTML
                        specifications, and deprecated features. Browse the list of tags and attributes from the main
                        menu. Enjoy!
                    </p>
                </div>
            </div>    
        );
};

/* creating App view */
var App = React.createClass({
        render : function() {
            return (
               <div>
                   <NavMenu />
                   <div className="container">
                        {this.props.children}
                   </div>
               </div>    
            );
        }
});

/* rendering app */
ReactDOM.render((
    <Router history={browserHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={Home}/>
            <Route path="tags" component={Tags}/>
            <Route path="tags/:detail" component={TagsDetail}/>
            <Route path="attributes" component={Attributes}/>
            <Route path="attributes/:detail" component={AttributesDetail}/>
            <Route path="idls" component={Idls}/>
            <Route path="idls/:detail" component={IdlsDetail}/>
        </Route>
    </Router>
), document.getElementById('app'));