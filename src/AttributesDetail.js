import React from "react";
import $ from "jquery";
import { Link} from "react-router";

/* creating AttributesDetail Class */
var AttributesDetail  = React.createClass({

    /* setting initial state */
    getInitialState : function () {
        return {
            name: "",
            description: "",
            defaultValue: "",
            validValues : "",
            categories: [],
            elements : [],
            examples: [],
            notes: [],
            recommendations: []
        };
    },

    /* getting data from api */
    componentDidMount: function () {
        var url = "/api/attributes/" + this.props.params.detail;

        this.serverRequest = $.get(url, function (result) {
            this.setState(result);
        }.bind(this));
    },

    render : function() {
        var attDetail = this.state;

        return (
            <div>

                {/* attribute name */}
                <h1>
                    <Link to="/attributes" className="no-underline">[</Link>
                    {attDetail.name}
                    <span className="giveMeLightBlue">]</span>
                </h1>

                {/* description */}
                <h2><small>Description</small></h2>
                <div className="well well-sm">
                    <span dangerouslySetInnerHTML={{__html:attDetail.description}} />
                </div>

                {/* valid values */}
                <h2><small>Valid Values</small></h2>
                <div className="well well-sm">
                    <span className="tag-fragment" dangerouslySetInnerHTML={{__html:attDetail.validValues}} />
                </div>

                {/* default value */}
                <h2><small>Default Value</small></h2>
                <div className="well well-sm">
                    {attDetail.defaultValue}
                </div>

                {/* examples */}
                {attDetail.examples.map( (elem, i) => (
                    <span key={elem.title + "_" + i}>
                        <h2><small>Example: {elem.title}</small></h2>
                        <pre>{elem.value}</pre>
                        <div className="well well-sm">
                            <span dangerouslySetInnerHTML={{__html:elem.value}} />
                        </div>
                    </span>)
                )}

                {/* notes */}
                {(attDetail.notes.length > 0) &&
                    <div>
                        <h2><small>Notes</small></h2>
                        <div className="well well-sm">
                            {attDetail.notes.map( (elem, i) => ( <span key={elem.value + "_" + i} dangerouslySetInnerHTML={{__html:elem.value}} />))}
                        </div>
                    </div>
                }

                {/* recommendations */}
                {(attDetail.recommendations.length > 0) &&
                    <div>
                        <h2><small>Recommendations</small></h2>
                        <div className="well well-sm">
                            {attDetail.recommendations.map( (elem, i) => ( < span key={elem.value + "_" + i} dangerouslySetInnerHTML={{__html:elem.value}} />))}
                        </div>
                    </div>
                }

                {/* elements */}
                <h2><small>Supported Elements</small></h2>
                <div className="well well-sm">
                    {attDetail.elements.map( (elem, i) => (<Link to={"/tags/" + elem} key={elem + "_" + i} className="button btn-primary btn-sm">{elem}</Link>))}
                </div>

            </div>
        );
    }
});

module.exports = AttributesDetail;
