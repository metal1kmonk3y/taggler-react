import React from "react";
import $ from "jquery";
import { Link} from "react-router";

/* creating Idls view */
var Idls = React.createClass({


    /* setting initial state */
    getInitialState : function () {
        return {
            idlsList: [],
            originalList: []
        };
    },

    filterList: function (event) {
        var updatedList = this.state.originalList;
        updatedList = updatedList.filter(function(name){
            return name.toLowerCase().search(
                    event.target.value.toLowerCase()) !== -1;
        });
        this.setState({idlsList: updatedList});
    },
    /* getting data from api */
    componentDidMount: function () {
        var url = "/api/idls/";

        this.serverRequest = $.get(url, function (result) {
            this.setState({
                idlsList: result.sort(),
                originalList: result.sort()
            });
        }.bind(this));
    },

    render : function() {
        var idlsNames = this.state.idlsList.map(function (name, i) {
            return(
                <li key={name + "_" + i} data-category={name}>
                    <Link to={"/idls/" + name}>
                        {"(" + name + ")"}
                    </Link>
                </li>

            );
        });

        return (
            <div>

                {/* idls */}
                <ul className="no-list-style">
                    <li><input type="text" placeholder="Search" onChange={this.filterList}/><br/><br/></li>
                    {idlsNames}
                </ul>
            </div>
        );
    }
});

module.exports = Idls;