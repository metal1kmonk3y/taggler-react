import React from "react";
import $ from "jquery";
import { Link} from "react-router";

/* creating Tags view */
var Tags = React.createClass({

    /* setting initial state */
    getInitialState : function () {
        return {
            tagsList: [],
            originalList: []
        };
    },

    filterList: function (event) {
        var updatedList = this.state.originalList;
        updatedList = updatedList.filter(function(tag){
            return tag.toLowerCase().search(
                    event.target.value.toLowerCase()) !== -1;
        });
        this.setState({tagsList: updatedList});
    },
    /* getting data from api */
    componentDidMount: function () {
        var url = "/api/tags/";

        this.serverRequest = $.get(url, function (result) {
            this.setState({
                tagsList: result.sort(),
                originalList: result.sort()
            });
        }.bind(this));
    },

    render : function() {
        var tagNames = this.state.tagsList.map(function (name, i) {
           return(
                <li key={name + "_" + i} data-category={name}>
                    <Link to={"/tags/" + name}>
                        {"<" + name + ">"}
                    </Link>
                </li>

           );
        });

        return (
            <div>

                {/* tags */}
                <ul className="no-list-style">
                    <li><input type="text" placeholder="Search" onChange={this.filterList}/><br/><br/></li>
                    {tagNames}
                </ul>
            </div>
        );
    }
});

module.exports = Tags;