import express from 'express';
import path from 'path';
import  {MongoClient} from 'mongodb';

const app = express();
const url = "mongodb://localhost:27017/taggler";
var db;

// Use connect method to connect to the mongo server
MongoClient.connect(url, function (err, database) {
    if(err) throw err;
    db = database;

    console.log(">> Database loaded.");
    /* listen on this port, currently 3000 */
    app.listen(process.env.PORT || 3000);
    console.log(">> Server started.\n");

});

/* server public directory's files as static */
app.use('/',express.static(path.join(__dirname, 'public')));

/* get method to serve request for api/tags/ */
app.get('/api/tags/', function(req, res){
    var names = [];

    db.collection('tag').find({}).toArray(function(err, results){
        if(err) return err;
        results.forEach(function (item) {
                names.push(item.name);
            }
        );
        res.json(names);
    })

});

app.get('/api/tags/:tag', function(req, res){
    var tagName = req.params.tag;

    db.collection('tag').findOne({name:tagName},function(err, result){
        if(err) return err;
        res.json(result);
    })
});

/* get method to serve request for api/attributes/ */
app.get('/api/attributes/', function(req, res){
    var names = [];

    db.collection('attributeDetails').find({}).toArray(function(err, results){
        if(err) return err;
        results.forEach(function (item) {
                names.push(item.name);
            }
        );
        res.json(names);
    })

});

app.get('/api/attributes/:attribute', function(req, res){
    var attributeName = req.params.attribute;

    console.log(attributeName);

    db.collection('attributeDetails').findOne({name:attributeName}, function(err, result){
        if(err) return err;
        res.json(result);

    })
});

/* get method to serve request for api/idls/ */
app.get('/api/idls/', function(req, res){
    var names = [];

    db.collection('idls').find({}).toArray(function(err, results){
        if(err) return err;
        results.forEach(function (item) {
                names.push(item.name);
            }
        );
        res.json(names);
    })

});

app.get('/api/idls/:idl', function(req, res){
    var idlsName = req.params.idl;

    db.collection('idls').findOne({name:idlsName}, function(err, result){
        if(err) return err;
        res.json(result);

    })
});